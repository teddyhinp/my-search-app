export const renderHiLightedName = (name, input) => {
    if (input) {
        const hiLightStart = name.toLowerCase().indexOf(input.toLowerCase());
        const highLightEnd = hiLightStart + input.length;
        return (
            <div className="nameRow">
                {name.slice(0, hiLightStart)}

                <span className="hiLightedText">{name.slice(hiLightStart, highLightEnd)}</span>

                {name.slice(highLightEnd, name.length)}
            </div>
        );
    } else {
        return <div>{name}</div>;
    }
};

export const renderDetail = (detail) => {
    return (
        <>
            <div>name: {detail.name}</div>
            <div>age: {detail.age}</div>
            <div>job: {detail.job}</div>
        </>
    );
};
