import React from 'react';

export default function Layout({ children }) {
    return (
        <div>
            <div className="header">
                <div className="textOverlay">Simple React Search App</div>
            </div>
            <div className="context">{children}</div>
        </div>
    );
}
