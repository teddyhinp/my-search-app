import React, { useState } from 'react';
import Layout from './components/Layout';
import NameItem from './components/NameItem';
import initList from './data';
import { renderDetail } from './helper';

function App() {
    const [input, setInput] = useState('');
    const [detail, setDetail] = useState({});
    const [searchResult, setSearchResult] = useState(initList);
    const [caseInsensitive, setCaseInsensitive] = useState(false);

    const handleSearch = (e) => {
        let value = e.target.value;
        setInput(value);
        let regex = caseInsensitive ? new RegExp(`${value}`, 'i') : new RegExp(`${value}`);
        let found = initList.filter((el) => el.name.match(regex));
        setSearchResult(found);
    };

    const handleCheckBoxChange = () => {
        setCaseInsensitive((prev) => !prev);
    };

    const handleNameOnClick = (name) => {
        let target = initList.filter((el) => el.name === name)[0];
        setDetail(target);
    };

    return (
        <Layout>
            <div className="resultContainer">
                <div>
                    <div className="inputBlock">
                        <input placeholder="Search" value={input} onChange={handleSearch} />
                        <div className="checkboxBlock">
                            <input type="checkbox" onChange={handleCheckBoxChange} />
                            <label>case-insensitive</label>
                        </div>
                    </div>
                    <NameList input={input} searchResult={searchResult} handleNameOnClick={handleNameOnClick} />
                </div>
                <div className="detailBox">{renderDetail(detail)}</div>
            </div>
        </Layout>
    );
}

export default App;

function NameList(props) {
    return (
        <div className="nameList">
            {props.searchResult.length > 0
                ? props.searchResult.map((el, i) => (
                      <div key={i} onClick={() => props.handleNameOnClick(el.name)} className="nameItem">
                          <NameItem name={el.name} input={props.input} />
                      </div>
                  ))
                : 'no result found'}
        </div>
    );
}
